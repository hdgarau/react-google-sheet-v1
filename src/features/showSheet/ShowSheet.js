import React, {useEffect, useRef, useState} from 'react';
import { useSelector } from 'react-redux';

import styles from "../showSheet/ShowSheet.module.css";
import {selectExcel} from "../init/dataSlice";
import {useParams} from "react-router";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";

export function ShowSheet() {
  const excel = useSelector(selectExcel);
    const { sheetName } = useParams();
    const [ data, setData ] = useState([] ||  excel[sheetName].all() );
    const  [ THs, setTHs ] = useState([] );
    const setDataByFilter = () =>
        setData(excel[sheetName].all()
            .filter( el => ( new RegExp(filtro.current.value,'i')).test(Object.values(el).join(' '))));
    const setTHsToShow = () =>
        setTHs(Object.keys( excel[sheetName].all()[0] ).filter(el => el[0] !== '_' ));

    let filtro = useRef(null);
    useEffect(
      (e)=> {
          excel && setDataByFilter() ;
          excel &&  setTHsToShow();
      },[  sheetName, excel ]);

    if(!excel) return ( <p>Cargando...</p>);

    const dynamicColumns = THs.map((col,i) => {
        return <Column key={'col' + i} field={col} header={col}/>;
    });

  return (
      <div>
        <div className={styles.row}>
          <h3>{sheetName}</h3>
        </div>
        <div className={styles.row}>
          <input
              ref={filtro}
              className={styles.textbox}
              aria-label="Filtro"
              placeholder={"Buscar..."}
              onKeyUp={setDataByFilter}
          />
        </div>
          <DataTable value={data}>
              {dynamicColumns}
          </DataTable>
      </div>
  );
}
