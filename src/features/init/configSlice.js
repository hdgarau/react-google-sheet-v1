import { createSlice } from '@reduxjs/toolkit';
import ConfigFile from "../../config";

export const configSlice = createSlice({
  name: 'config',
  initialState: ConfigFile,
  reducers: {
  },
});

export const Config = state => state.config;

export default configSlice.reducer;
