import { createSlice } from '@reduxjs/toolkit';
import Tabletop from "tabletop";
import { Config } from "./configSlice";
import {useSelector} from "react-redux";

const elementsMapped = (toSearch, el,val, carry = '') => {
    let curr = el.shift();
    if(el.length === 0)
    {
        if(!toSearch[curr] )
            toSearch[curr] ={};
        return toSearch[curr][val] = 'show/' + carry + '(' + curr + ')' + val;
    }
    if(!toSearch[curr])
        toSearch[curr] = {};
    return elementsMapped( toSearch[curr], el, val, '(' + curr + ')');
};
const setIndexes = (arr) => {
    let options = {};
    for(let el of arr)
    {
        let regex = /^\((([A-Z 0-9])|(\)\())+\)/ig;
        let val = el.replace(regex,'');
        if(!regex.test(el))
        {
            options[el] = 'show/' + val;
            continue;
        }

        let str = el.match(regex)[0];
        let arr = str.substr(1,str.length -2).split(')(');
        elementsMapped(options, arr,val);
    }

    return options;
};
export const dataSlice = createSlice({
  name: 'data',
  initialState: {
      excel : null,
      options : []
  },
  reducers: {
      set: (state, action) => {
        state.excel =  action.payload;
          console.log( setIndexes ((Object.keys( action.payload )
              .filter( el => ! (/^_/).test(el))))
          );
        state.options =  setIndexes ((Object.keys( action.payload )
            .filter( el => ! (/^_/).test(el)))
            );
      },
  },
});
export const { set } = dataSlice.actions;

export const init = () => dispatch => {

    const key = useSelector(Config).excel_key;

        Tabletop.init({
            key: key,
            simpleSheet: false
        })
            .then((data) => dispatch(set(data)))
            .catch((err) => console.warn(err));
    //return true;
};

export const selectExcel = state => state.data.excel;
export const selectOptions = state => state.data.options;

export default dataSlice.reducer;
