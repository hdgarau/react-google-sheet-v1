import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import { useSelector} from "react-redux";
import {selectOptions} from "../init/dataSlice";
import style from './NavBar.module.css'
export function NavBar() {
    const [options, setOptions] = useState( []);
    const optionsSelector = useSelector(selectOptions);
    const buildNavBar = (el) => {
        let htmlLis =  [];

        for(let title in el)
        {
            htmlLis.push( typeof el[title] ==="string" ?
                (<li><Link to={'/' + el[title]} >{title}</Link> </li>) :
                (<ul className={style.row}><h4>{title}</h4> {buildNavBar(el[title])} </ul>));
        }
        return htmlLis;
    }
    useEffect(()=>{  setOptions(buildNavBar(optionsSelector))}  , [optionsSelector]);

    return (
        <nav>
            <ul className={style.row}>
                {  options.map(el=>el) }
            </ul>
        </nav>
    );
}

