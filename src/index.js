import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import store from './app/store';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';

// sincronizamos el browserHistory de React Router con el Store
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
        {/*le decimos al Router que use nuestro history sincronizado*/}
        <App/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
