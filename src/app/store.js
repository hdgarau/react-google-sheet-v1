import { configureStore } from '@reduxjs/toolkit';
import configReducer from '../features/init/configSlice';
import dataReducer from '../features/init/dataSlice';


export default configureStore({
  reducer: {
    config: configReducer,
    data: dataReducer,
  },
});
