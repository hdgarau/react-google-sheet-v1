import './App.css';
import { ShowSheet } from './features/showSheet/ShowSheet';
import { NavBar } from "./features/navBar/NavBar";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Config} from "./features/init/configSlice";
import {init as initData} from "./features/init/dataSlice";


function App() {
    const config = useSelector(Config);
    const dispatch = useDispatch();
    dispatch(initData());
  return (
    <div className="App" >

      <BrowserRouter>
      <header className="App-header">
          <h1>{config.title}</h1>
        <NavBar/>
        <Switch>
          <Route path="/show/:sheetName" component={ShowSheet}  />
        </Switch>
      </header>
      </BrowserRouter>
    </div>
  );
}

export default App;